FROM ruby:2.5.1

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs

ENV APP_HOME /ndh_seller_api
ENV BUNDLER_VERSION 2.1.2
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

ENV BUNDLE_PATH /box
ENV GEM_PATH /box
ENV GEM_HOME /box

ADD . $APP_HOME

COPY Gemfile /ndh_seller_api/Gemfile

#COPY Gemfile.lock /ndh_seller_api/Gemfile.lock

RUN gem update --system
RUN gem install bundler:2.1.2
#RUN gem install bundler -v 2.1.2 --default

RUN bundle install

COPY . /ndh_seller_api